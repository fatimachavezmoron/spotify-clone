import React from 'react'
import logo from '../../assets/images/spotifyLogo.png'
import './Login.css'
import { accessUrl } from "../Spotify";


const Login = () => {
  return (
    <div className='login'>
      <img src={logo} alt='spotify-logo' className='spotifyLogo'/>
      <a href={accessUrl}>Login with Spotify</a>
    </div>
  )
}

export default Login