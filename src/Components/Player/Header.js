import React from 'react'
import './Header.css'
import search from '../../assets/images/search.svg'
import { ChakraProvider } from '@chakra-ui/react'
import { Avatar } from '@chakra-ui/react'
import { useDataLayerValue } from '../DataLayer/DataLayer'
// import { Search2Icon, AddIcon, WarningIcon } from '@chakra-ui/icons'


const Header = () => {
  const [{ user}] = useDataLayerValue();
  return (
    <ChakraProvider>
    <div className='header'>
      <div className='header_left'>

      <img src={search} alt='homeIcon' className='search__Icon'/>
      <input 
        placeholder='Search artist, song, album'
        type='text'
        className='placeholder'
      />
      </div>
      <div className='header_right'>
        <Avatar alt={user?.display_name} src='https://bit.ly/broken-link'
        size='sm' className='avatar'/>
      <h4>{user?.display_name}</h4>
      </div>
    </div>
    </ChakraProvider>
  )
}

export default Header