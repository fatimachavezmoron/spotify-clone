import React from 'react'
import './Body.css'
import Header from './Header'
import { useDataLayerValue } from '../DataLayer/DataLayer'
import play from '../../assets/images/playgreen.svg'
import heart from '../../assets/images/heart.svg'
import doots from '../../assets/images/doots.svg'
import SongRow from './SongRow'

function Body({spotify}) {
  const [{discover_weekly}] =useDataLayerValue();
  
  return (
    <div className='body'>
      <Header spotify={spotify}/>
      <div className='body__info'>
        <img src={discover_weekly?.images[0].url} alt='discoverImg' className='discoverImg'/>
        <div className='body__infoText'>
        <strong>PLAYLIST</strong>
        <h2>Discover <br/> Weekly</h2>
        <p>{discover_weekly?.description}</p>
        </div>
      </div>
      <div className='body__songs'>
          <div className='body__icons'> 
            <img src={play} alt='playIcon' className='playIcon bodyIcon'/>
            <img src={heart} alt='playIcon' className='heartIcon bodyIcon'/>
            <img src={doots} alt='playIcon' className='dootsIcon bodyIcon'/>
          </div>
          {discover_weekly?.tracks.items.map(item =>
            <SongRow track={item.track} />
            )}
        </div>
    </div>
  )
}

export default Body
