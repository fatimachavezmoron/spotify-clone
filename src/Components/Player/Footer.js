import React from 'react'
import './Footer.css'
import { ChakraProvider } from '@chakra-ui/react'
import {
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
} from '@chakra-ui/react'
import shuffle from '../../assets/images/shuffle.svg'
import play from '../../assets/images/play.svg'
import repeat from '../../assets/images/repeat.svg'
import skipNext from '../../assets/images/skipNext.svg'
import skipPrevious from '../../assets/images/skipPrevious.svg'
import volume from '../../assets/images/volume.svg'
import elderbrook from '../../assets/images/elderbrook.png'
import screen from '../../assets/images/screen.svg'
import greenHeart from '../../assets/images/greenHeart.svg'

const Footer = () => {
  return (
    <ChakraProvider>
    <div className='footer'>
      <div className='footer__left'>
        <img src={elderbrook} alt='imgEdelbrook' className='footer__albumlogo'/>
        <div className='titleCont'>
        <h4>Little love</h4> 
        <p>Edelbroook</p>
        </div>
        <img src={greenHeart} alt='shuffleIcon' className='icons__footerLeft'/>
        <img src={screen} alt='skipPIcon' className='icons__footerLeft'/>
      </div>
      <div className='footer__center'>
        <img src={shuffle} alt='shuffleIcon' className='icons'/>
        <img src={skipPrevious} alt='skipPIcon' className='icons'/>
        <img src={play} alt='playIcon' className='icons' />
        <img src={skipNext} alt='skipNextIcon' className='icons'/>
        <img src={repeat} alt='repeatIcon' className='icons'/>
      </div>
      <div className='footer__right'>
      <img src={volume} alt='volumeIcon' />
      <Slider w={170} defaultValue={60} min={0} max={200} step={30}>
        <SliderTrack bg='white'>
          <SliderFilledTrack bg='#30d567' />
        </SliderTrack>
        <SliderThumb boxSize={3} />
      </Slider>
      </div>      
    </div>
    </ChakraProvider>
  )
}

export default Footer