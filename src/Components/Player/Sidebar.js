import React from 'react'
import './Sidebar.css'
import spotify from '../../assets/images/spotifyLogo.png'
import SidebarOption from './SidebarOption'
import home from '../../assets/images/home.png'
import search from '../../assets/images/search.png'
import library from '../../assets/images/library.png'
import { useDataLayerValue } from '../DataLayer/DataLayer';

const Sidebar = () => {
  const [{playlist}] = useDataLayerValue();
  return (
    <div className='sidebar'>
      <img src={spotify} alt='logo-sidebar' className='sidebar__logo'/>
      <div className='sidebarOptionCont'>
        <div className='itemsCont'>
        <img src={home} alt='homeIcon' className='sidebarIcon'/>
        <SidebarOption  title='Home'/>
        </div>
        <div className='itemsCont'>
        <img src={search} alt='homeIcon' className='sidebarIcon'/>
        <SidebarOption  title='Search'/>
        </div>
        <div className='itemsCont'>
        <img src={library} alt='homeIcon' className='sidebarIcon'/>
        <SidebarOption title='Your Library'/>
        </div>
      </div>
      <br />
        <strong className='sidebar__title'>PLAYLIST</strong>
        <hr/>
        {playlist?.items?.map(playlist => (
          <SidebarOption title={playlist.name}/>
        ))}
        <SidebarOption title='Rock'/>
        <SidebarOption title='Pop'/>
        <SidebarOption title='Hip Hop'/>
        <SidebarOption title='Deep House'/>
    </div>
  )
}

export default Sidebar