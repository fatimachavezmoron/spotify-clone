export const initialState = {
  user: null,
  playlist:[],
  playing: false,
  item:null,
  // token:'BQDPzGZ2QHqg-8i3niys6UvlSkU5cmyL_IGOcb0ufPpWnrHLGqbgCGmlTdUAz1AkMPrTxSfGNLPRogLm-9u6P-wMRUP6rtKxGTNQee1-v4T3TL0L7MDOb9DjNvPK7Ui8N-mXySzvzajh5JfTrWVEpTkyo7WMoLLSKuV86jzwMynDCmmmKPu2a41LybfRjNqX15iv8Nl6TRw6Bylj',
};

const reducer = (state, action) => {
  console.log(action);

  switch(action.type) {
    case 'SET_USER':
      return {
        ...state,
        user: action.user
      };
    case 'SET_TOKEN':
      return {
        ...state,
        token: action.token  
      };
    case 'SET_PLAYLISTS':
      return {
        ...state,
        playlist: action.playlist,
      };
    case 'SET_DISCOVER_WEEKLY':
      return {
        ...state,
        discover_weekly: action.discover_weekly,
      }    
      default: 
      return state;
  }
}

export default reducer;